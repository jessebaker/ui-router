var express = require('express'),
	serveStatic = require('serve-static'),
	bodyParser = require('body-parser'),
	app = express(),
	invoices = [
		{"id":0,"date":"2015-05-12T13:40:00.000Z","description":"Unit of 40 spearmen","amount":3000.00,"paid":true},
		{"id":1,"date":"2015-05-12T14:40:00.000Z","description":"4 barrels fine wine","amount":289.00,"paid":false},
		{"id":2,"date":"2015-05-12T14:40:00.000Z","description":"4 barrels ale","amount":200.00,"paid":false},
		{"id":3,"date":"2015-05-12T14:40:00.000Z","description":"New scrolls","amount":207.00,"paid":true},
		{"id":4,"date":"2015-05-15T14:40:00.000Z","description":"Set of steps","amount":25.00,"paid":false},
		{"id":5,"date":"2015-05-16T14:40:00.000Z","description":"Sellsword hire","amount":99.99,"paid":false}
	];
app.use(serveStatic(process.cwd()));
app.get('/api/invoices', function (req, res) {
	res.json(invoices);
});
app.get('/api/invoices/:id', function (req, res) {
	res.json(invoices[req.params.id]);
});
app.post('/api/invoices', bodyParser.json(), function (req, res) {
	var invoice = req.body;
	invoice.id = invoices.length;
	invoices.push(invoice);
	res.json(invoice);
});
app.put('/api/invoices/:id', bodyParser.json(), function (req, res) {
	var invoice = req.body;
	if (!req.params.id in invoices) {
		res.status(500)
		   .end('Invoice ' + req.params.id + ' not found');
	}
	invoice.id = req.params.id;
	invoices[req.params.id] = req.body;
	// res.status(500) // Test error handling
	res.json(invoice);
});
app.listen(3000, function () {
	console.log('Listening on port 3000');
});
