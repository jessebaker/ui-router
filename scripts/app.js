'use strict';
(function(){
	angular
		.module('crunchLite', [
			'ui.router',
			'ngMessages',
			'ngResource',
			'angular-datepicker'
		])
		.config(Config)
		.run(AppRun);

	Config.$inject = ['$compileProvider', '$locationProvider', '$stateProvider', '$urlRouterProvider'];
	function Config(   $compileProvider,   $locationProvider,   $stateProvider,   $urlRouterProvider) {
		$compileProvider.debugInfoEnabled(false);
		//TODO HTML5 mode would be cleaner but the server needs to be set up to always server index.html
		$locationProvider
			.html5Mode({
				enabled: false,
				requireBase: true
			})
			.hashPrefix('!')
		;

		$stateProvider
			.state('app', {
				abstract: true,
				templateUrl: 'views/layout.html',
				controller: 'PageCtrl',
				controllerAs: 'pagevm'
			})
			.state('app.invoices', {
				url: '/invoices',
				templateUrl: 'views/main.html',
				controller: 'MainCtrl',
				controllerAs: 'vm',
				resolve : {
					invoices : GetInvoiceList
				}
			})
			.state('app.invoice_create', {
				url: '/invoice/new',
				templateUrl: 'views/invoice/manage.html',
				controller: 'InvoiceManageCtrl',
				controllerAs: 'vm',
				resolve : {
					invoice : GetInvoice
				}
			})
			.state('app.invoice_edit', {
				url: '/invoice/edit/:id',
				templateUrl: 'views/invoice/manage.html',
				controller: 'InvoiceManageCtrl',
				controllerAs: 'vm',
				resolve : {
					invoice : GetInvoice
				}
			});

		$urlRouterProvider.otherwise('/invoices');

		GetInvoiceList.$inject = ['$q', 'InvoiceService', 'InvoiceModel'];
		function GetInvoiceList($q, InvoiceService, InvoiceModel) {
			var defer = $q.defer();

			InvoiceService
				.query()
				.$promise
				.then(function(data){
					var list = [];
					angular.forEach(data, function(val){
						list.push(new InvoiceModel(val));
					});
					defer.resolve(list);
				});

			return defer.promise;
		}

		GetInvoice.$inject = ['$q', 'InvoiceService', 'InvoiceModel', '$stateParams'];
		function GetInvoice($q, InvoiceService, InvoiceModel, $stateParams) {
			var defer = $q.defer();

			if($stateParams.id) {
				InvoiceService
					.get({invoiceId: $stateParams.id})
					.$promise
					.then(function(data){
						defer.resolve(new InvoiceModel(data));
					});
			} else {
				defer.resolve(new InvoiceModel());
			}

			return defer.promise;
		}
	}

	AppRun.$inject = [ '$rootScope'];
	function AppRun(	$rootScope) {
		$rootScope.pageLoading = true;

		$rootScope.$on('$stateChangeStart', function() {
			$rootScope.pageLoading = true;
		});

		$rootScope.$on('$stateChangeSuccess', function() {
			$rootScope.pageLoading = false;
		});
	}

})();