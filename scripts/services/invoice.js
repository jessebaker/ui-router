'use strict';

(function(){
	angular
		.module('crunchLite')
		.service('InvoiceModel', function(){
			var Invoice = function(data) {
				if (data === undefined) {
					data = {
						date: new Date()
					};
				}
				this.load(data);
			};

			Invoice.prototype.load = function(data) {
				this.id = data.id;
				this.description = data.description;
				this.date = new Date(data.date);
				this.amount = data.amount;
				this.paid = data.paid;
			};

			Invoice.prototype.toArray = function() {
				return {
					description : this.description,
					date	 	: this.date,
					amount	 	: this.amount,
					paid	 	: this.paid
				};
			};

			return Invoice;
		})
		.service('InvoiceService', ['$resource', function($resource){
			return $resource('api/invoices/:invoiceId',{invoiceId: '@id'}, {
				query: {
					method: 'GET',
					isArray: true
				},
				get: {
					method:'GET'
				},
				update: {
					method:'PUT'
				},
				save: {
					method:'POST'
				}
			});
		}]);
})();
