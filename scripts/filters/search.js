'use strict';

(function(){
	angular
		.module('crunchLite')
		.filter('searchFilter', function($filter){
			return function(items,filter) {
				var found = $filter('filter')(items,{description: filter.search});
				var result = [];
				angular.forEach(found, function(item) {
					if( filter.paid === item.paid ||
						filter.paid === null) {
						result.push(item);
					}
				});
				return result;
			}
		});
})();
