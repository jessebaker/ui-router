'use strict';

(function() {
	angular.module('crunchLite')
		.controller('PageCtrl', pageController);

	pageController.$inject = ['$state'];
	function pageController  ( $state) {
		var pagevm = this;

		//TODO, get user from backend.
		pagevm.currentUser = {
			id: 0,
			name: 'Tyrion Lannister'
		};

		pagevm.currentDate = Date.now();

		pagevm.stateName = $state.current.name;

	}
})();