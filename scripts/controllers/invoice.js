'use strict';

(function() {
	angular.module('crunchLite')
		.controller('InvoiceManageCtrl', invoiceController);

	invoiceController.$inject =['invoice', '$rootScope', '$state', '$filter', 'InvoiceService'];
	function invoiceController(  invoice,   $rootScope,   $state,   $filter,   InvoiceService) {
		var vm = this;
		vm.invoice = invoice;
		vm.editing = invoice.id !== undefined; //If the invoice doesn't have an ID it's an empty model so this must be a new invoice.
		vm.dateOptions = {
			format:'yyyy-mm-dd',
			max: Date.now()
		};
		vm.saveInvoice = saveInvoice;

		function saveInvoice() {
			$rootScope.pageLoading = true;
			if(vm.invoiceForm.$valid) {
				vm.invoice.date = $filter('date')(new Date(vm.invoice.date), 'yyyy-MM-ddTHH:mm:ssZ');
				if(vm.editing) {
					InvoiceService
						.update(
							vm.invoice,
							onSuccess,
							onFail
						);
				} else {
					InvoiceService
						.save(
							vm.invoice,
							onSuccess,
							onFail
						);
				}
			}
			function onSuccess(data) {
				$state.go('app.invoices');
			}
			function onFail(error){
				console.error('Fail', error.statusText);
				alert(error.statusText);
				$rootScope.pageLoading = false;
			}
		}
	}
})();
