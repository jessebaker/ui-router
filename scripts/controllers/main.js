'use strict';

(function() {
	angular.module('crunchLite')
		.controller('MainCtrl', invoiceListController);

	invoiceListController.$inject =['invoices', 'InvoiceService', 'InvoiceModel'];
	function invoiceListController(  invoices,   InvoiceService,   InvoiceModel) {
		var vm = this;
		vm.invoices = invoices;
		vm.markInvoicePaid = markInvoicePaid;
		vm.filters = {
			paid : null,
			search: ''
		};
		vm.filterOptions = [
			{
				label: 'All',
				value: null
			},
			{
				label: 'Paid',
				value: true
			},
			{
				label: 'Unpaid',
				value: false
			}
		];

		function markInvoicePaid(invoice) {
			//To make UI seem snappy, update model locally.
			invoice.paid = !invoice.paid;
			InvoiceService
				.update(
					invoice,
					function success(data) {
						for(var i=0; i<vm.invoices.length; i++) {
							//data.id is a string so use ==
							if(vm.invoices[i].id == data.id) {
								vm.invoices[i] = new InvoiceModel(data);
							}
						}
					},
					function fail(error){
						console.error('Fail', error.statusText);
						//The request failed, unset the change.
						invoice.paid = !invoice.paid;
					});
		}
	}
})();
