## Scenario

A web application in Angular, Angular UI-Router and Node. It allows its users to track their invoices. 

## Requirements

* As a small business I want to
	* see a list of my invoices
	* record a new invoice
	* edit an invoice
	* mark an invoice as paid or unpaid
	* filter invoices by paid and unpaid

## Considerations

The business plans to grow this app's features into a much larger app, so code must be maintainable by other developers and be robust to changes in requirements.

## Getting Started

You'll need [nodejs and npm](https://nodejs.org/download) installed to get started, then run:

```
npm install
npm start
```

And navigate to http://localhost:3000.

## API endpoints

*** Note: you must specify the `Content-Type: application/json` for POST and PUT ***

### `GET /api/invoices`

Gets a list of all invoices

### `GET /api/invoices/{id}`

Gets a specific invoice

### `POST /api/invoices`

Creates a new invoice

### `PUT /api/invoices/{id}`

Updates a specific invoice